# Use a base image with Java 20
FROM openjdk:20-jdk-slim
#VOLUME /tmp
MAINTAINER haritech.com
ARG JAR_FILE=build/libs/data-service.jar
#WORKDIR /opt/app
#COPY ${JAR_FILE} /var/lib/jenkins/workspace/spring-gradle-docker
ADD ${JAR_FILE} data-service.jar
ENTRYPOINT ["java","-jar","data-service.jar"]
EXPOSE 8081

#docker build -t data-service:2 .

docker run \
  --name spring-container \
  --network spring-mysql-network \
  --env=SPRING_DATASOURCE_PRIMARY_URL=jdbc:mysql://mysql8-container:3306/hosp_mgmt \
  --env=SPRING_DATASOURCE_PRIMARY_USERNAME=root \
  --env=SPRING_DATASOURCE_PRIMARY_PASSWORD=dudkrish1A \
  --env=SPRING_DATASOURCE_ORDER_URL=jdbc:mysql://mysql8-container:3306/practice \
  --env=SPRING_DATASOURCE_ORDER_USERNAME=root \
  --env=SPRING_DATASOURCE_ORDER_PASSWORD=dudkrish1A \
  -p 8081:8081 \
  data-service:2