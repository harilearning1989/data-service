package com.web.demo.records;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RsvEmpRecord(
    int empId,
    String empName,
    String fatherName,
    double salary,
    String mobile,
    String gender) {}
