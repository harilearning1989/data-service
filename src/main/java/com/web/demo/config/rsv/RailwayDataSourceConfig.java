package com.web.demo.config.rsv;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "rsvEntityManagerFactory",
        transactionManagerRef = "rsvTransactionManager",
        basePackages = {"com.web.demo.repos.rsv"})
public class RailwayDataSourceConfig {

    @Bean(name = "rsvDataSourceProperties")
    @ConfigurationProperties("spring.datasource.rsv")
    public DataSourceProperties rsvDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "rsvDataSource")
    @ConfigurationProperties("spring.datasource.rsv.configuration")
    public DataSource rsvDataSource(
            @Qualifier("rsvDataSourceProperties") DataSourceProperties rsvDataSourceProperties) {
        return rsvDataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
    }

    @Bean(name = "rsvEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean rsvEntityManagerFactory(
            EntityManagerFactoryBuilder rsvEntityManagerFactoryBuilder,
            @Qualifier("rsvDataSource") DataSource rsvDataSource) {

        return rsvEntityManagerFactoryBuilder
                .dataSource(rsvDataSource)
                .packages("com.web.demo.models.rsv")
                .persistenceUnit("rsvDataSource")
                .build();
    }

    @Bean(name = "rsvTransactionManager")
    @ConfigurationProperties("spring.jpa")
    public PlatformTransactionManager rsvTransactionManager(
            @Qualifier("rsvEntityManagerFactory") EntityManagerFactory rsvEntityManagerFactory) {
        return new JpaTransactionManager(rsvEntityManagerFactory);
    }
}
