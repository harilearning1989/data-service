package com.web.demo.services;

import java.net.URISyntaxException;

public interface CommonService {
    void retryExample() throws URISyntaxException;
}
