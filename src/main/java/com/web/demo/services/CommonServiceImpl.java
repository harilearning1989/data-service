package com.web.demo.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Service
@EnableRetry
@Slf4j
public class CommonServiceImpl implements CommonService {

    @Autowired
    RestTemplate restTemplate;

    @Override
    @Retryable(retryFor = { Exception.class },
            maxAttempts = 5,
            backoff = @Backoff(delay = 2000, multiplier = 2, maxDelay = 2000))
    public void retryExample() throws URISyntaxException {
        log.info("Connecting to Server");
        ResponseEntity<String> responseFromServer = restTemplate.exchange(new URI("http://localhost:9001/generate"),
                HttpMethod.GET, null, String.class);
        log.info(String.format("Response recieved from Server: %s", responseFromServer.getBody()));
    }

    @Recover
    public String recoverSaveTodo(){
        log.info("recovered to Server");
        return "Hello";
    }

   /* @Retryable(
            retryFor = { Exception.class, IllegalArgumentException.class },
            maxAttempts = 3,
            backoff = @Backoff(delay = 800))
    public String saveTodo() throws Exception, IllegalArgumentException {
        return "todo saved.";
    }*/

}
