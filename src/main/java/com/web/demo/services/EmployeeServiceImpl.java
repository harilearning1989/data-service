package com.web.demo.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.demo.models.order.Student;
import com.web.demo.models.rsv.Employee;
import com.web.demo.records.RsvEmpRecord;
import com.web.demo.records.StudentRecord;
import com.web.demo.repos.rsv.EmployeeRepository;
import com.web.demo.utils.CommonUtils;
import com.web.demo.utils.DownloadFileUtil;
import com.web.demo.utils.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    JsonUtil jsonUtil;

    @Autowired
    public EmployeeServiceImpl setJsonUtil(JsonUtil jsonUtil) {
        this.jsonUtil = jsonUtil;
        return this;
    }

    @Override
    public List<Employee> getAllEmployees() {
        List<Employee> employeeList = employeeRepository.findAll();
        return Optional.ofNullable(employeeList)
                .orElseGet(Collections::emptyList)
                .stream()
                .limit(100)
                .toList();
    }

    @Override
    public Optional<Employee> getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public List<Employee> saveAll() {
        String employeeFile = "json/EmployeeData.json";
        try {
            DownloadFileUtil.downloadFile(employeeFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<RsvEmpRecord> employeeRecords = null;
        List<Employee> employeeList = null;

        String fixture = null;

        String filePath = CommonUtils.fileLocation() + employeeFile;
        ///Users/hariduddukunta/MyWork/DataFiles/json/EmployeeData.json
        employeeRecords =
                CommonUtils.readJsonFileFromPath(filePath, new TypeReference<List<RsvEmpRecord>>() {
                });
        if (null != employeeRecords && employeeRecords.isEmpty()) {
            System.out.println("No employees found or there was an error.");
        }
            /*employeeRecords = objectMapper.readValue(fixture,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, RsvEmpRecord.class));*/
        employeeList = convertRecordToEntity(employeeRecords);

        employeeList = employeeRepository.saveAll(employeeList);
        return employeeList;
    }

    private List<Employee> convertRecordToEntity(List<RsvEmpRecord> employeeRecords) {
        return employeeRecords
                .stream()
                .map(m -> {
                    Employee.EmployeeBuilder employeeBuilder = Employee.builder();
                    employeeBuilder.empId(m.empId())
                            .empName(m.empName())
                            .phone(m.mobile())
                            .gender(m.gender())
                            .salary(m.salary());

                    String username = "";
                    if (m.empName().contains(" ")) {
                        username = m.empName().replaceAll(" ", "_").toLowerCase();
                    }
                    employeeBuilder.username(username);
                    employeeBuilder.email(username + "@gmail.com");
                    employeeBuilder.age(CommonUtils.randomNumberBetween(20, 70));

                    Employee employee = employeeBuilder.build();
                    return employee;
                })
                .toList();

    }
}
