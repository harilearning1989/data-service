package com.web.demo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.web.demo.constants.ApplicationConstants;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CommonUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String getOsName() {
        return System.getProperty("os.name");
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isLinux() {
        return getOsName().startsWith("Linux");
    }

    public static boolean isMac() {
        return getOsName().startsWith("Mac");
    }

    public static String fileLocation() {
        if (CommonUtils.isWindows()) {
            return ApplicationConstants.DOWNLOAD_LOCATION;
        } else if (CommonUtils.isLinux()) {
            return ApplicationConstants.DOWNLOAD_LOCATION;
        } else if (CommonUtils.isMac()) {
            return ApplicationConstants.DOWNLOAD_MAC;
        }
        return null;
    }

    public static String readResourceFilePath(final String FILE_PATH, Charset charset) throws IOException {
        String text = new String(Files.readAllBytes(Paths.get(FILE_PATH)), StandardCharsets.UTF_8);
        return Resources.toString(Resources.getResource(text), charset);
    }

    public static String readResource(final String fileName, Charset charset) throws IOException {
        return Resources.toString(Resources.getResource(fileName), charset);
    }

    public static <T> T readJsonFileFromPathSingle(String filePath, final TypeReference<T> typeReference) {
        try {
            return new ObjectMapper().readValue(new File(filePath), typeReference);
        } catch (JsonProcessingException e) {
            System.err.println("Error reading JSON file: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    public static <T> List<T> readJsonFileFromPath(String filePath, TypeReference<List<T>> typeReference) {
        try {
            return objectMapper.readValue(new File(filePath), typeReference);
        } catch (IOException e) {
            System.err.println("Error reading JSON file: " + e.getMessage());
        }
        return Collections.emptyList(); // Return an empty list if there's an error
    }

    public static int randomNumberBetween(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

}
