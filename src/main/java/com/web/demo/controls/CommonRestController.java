package com.web.demo.controls;

import com.web.demo.services.CommonService;
import com.web.demo.services.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("common")
public class CommonRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonRestController.class);

    CommonService commonService;

    @Autowired
    public CommonRestController setCommonService(CommonService commonService) {
        this.commonService = commonService;
        return this;
    }

    @GetMapping("retryExample")
    public String retryExample() throws IOException, URISyntaxException {
        LOGGER.info("saveAllPatients");
        commonService.retryExample();
        return "Hello World";
    }
}
