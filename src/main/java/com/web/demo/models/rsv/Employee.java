package com.web.demo.models.rsv;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "EMPLOYEE")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EMP_ID")
    private int empId;
    @Column(name = "EMP_NAME")
    private String empName;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "AGE")
    private int age;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "SALARY")
    private double salary;
    @Column(name = "DATE_CREATED")
    private String createdDate;
    @Column(name = "LAST_UPDATED")
    private String lastUpdatedDate;
}
