package com.web.demo.models.order;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "STUDENT_DETAILS")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STUDENT_ID")
    private Long studentId;
    @Column(name = "STUDENT_NAME")
    private String studentName;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "DOB")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(name = "FATHER_NAME")
    private String fatherName;
    @Column(name = "BLOOD_GROUP")
    private String bloodGroup;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDR_IDS")//, referencedColumnName = "ADDR_ID")
    private StudentAddress address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "STUDENT_IDS")
    private Set<StudentSubject> subjects;

}
