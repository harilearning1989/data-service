package com.web.demo;

import com.web.demo.utils.DateFormatterUtil;
import org.junit.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import static org.junit.Assert.*;

public class DateFormatterUtilTest {

    @Test
    public void testConvertStringToDate() throws Exception {
        String dateStr = "2023-12-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date expectedDate = formatter.parse(dateStr);
        Date result = DateFormatterUtil.convertStringToDate(dateStr);
        assertEquals(expectedDate, result);
    }

    @Test
    public void testConvertStringToDateTime() {
        String dateTimeStr = "2023-12-01 12:00";
        LocalDateTime expectedDateTime = LocalDateTime.of(2023, 12, 1, 12, 0);
        LocalDateTime result = DateFormatterUtil.convertStringToDateTime(dateTimeStr);
        assertEquals(expectedDateTime, result);
    }

    @Test
    public void testConvertStringToDateWithInvalidFormat() {
        String dateStr = "invalid-date";
        Date result = DateFormatterUtil.convertStringToDate(dateStr);
        assertNull(result);
    }

    @Test
    public void testConvertStringToDateTimeWithInvalidFormat() {
        String dateTimeStr = "invalid-date-time";
        try {
            DateFormatterUtil.convertStringToDateTime(dateTimeStr);
        } catch (Exception e) {
            assertEquals(java.time.format.DateTimeParseException.class, e.getClass());
        }
    }

    @Test
    public void testIsValidDateFormat() {
        // Test the isValidDateFormat method with a valid date string
        assertTrue(DateFormatterUtil.isValidDateFormat("2023-12-01"));
        // Test the isValidDateFormat method with an invalid date string
        assertFalse(DateFormatterUtil.isValidDateFormat("invalid-date"));
    }

    @Test
    public void testIsValidDateTimeFormat() {
        // Test the isValidDateTimeFormat method with a valid date-time string
        assertTrue(DateFormatterUtil.isValidDateTimeFormat("2023-12-01 12:00"));

        // Test the isValidDateTimeFormat method with an invalid date-time string
        assertFalse(DateFormatterUtil.isValidDateTimeFormat("invalid-date-time"));
    }

    @Test
    public void testConvertStringToDateTest() {
        // Test the convertStringToDate method with a valid date string
        String dateStr = "2023-12-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date expectedDate = formatter.parse(dateStr);
            Date result = DateFormatterUtil.convertStringToDate(dateStr);
            assertEquals(expectedDate, result);
        } catch (ParseException e) {
            fail("Test failed due to ParseException");
        }
        // Test the convertStringToDate method with an invalid date string
        Date result = DateFormatterUtil.convertStringToDate("invalid-date");
        assertNull(result);
    }

    @Test
    public void testConvertDateTimeToString() {
        // Test the convertDateTimeToString method with a valid LocalDateTime object
        LocalDateTime dateTime = LocalDateTime.of(2023, 12, 1, 12, 0);
        String expectedStr = "2023-12-01 12:00";
        String result = DateFormatterUtil.convertDateTimeToString(dateTime);
        assertEquals(expectedStr, result);

        // Test the convertDateTimeToString method with null
        result = DateFormatterUtil.convertDateTimeToString(null);
        assertNull(result);
    }
}
