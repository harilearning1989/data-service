# Specify the Docker image with the JDK, Maven, and kubectl installed
image: docker:latest

# Define stages for the pipeline
stages:
  - build
  - test
  - quality
  - package
  - deploy

# Define variables
variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  SONAR_HOST_URL: "https://sonarcloud.io"
  SONAR_TOKEN: "your_sonar_token"
  DOCKER_HOST: "tcp://docker:2375"
  KUBE_NAMESPACE: "your_kubernetes_namespace"
  KUBE_SERVER: "https://your_kubernetes_server"
  KUBE_CA_PEM: "your_kubernetes_ca.pem"
  KUBE_TOKEN: "your_kubernetes_token"
  KUBE_CLUSTER_NAME: "your_kubernetes_cluster_name"
  KUBE_DEPLOYMENT_NAME: "your_kubernetes_deployment_name"

# Cache Maven dependencies to speed up builds
cache:
  paths:
    - .m2/repository

# Job to build the project
build:
  stage: build
  script:
    - mvn clean package

# Job to run unit tests
test:
  stage: test
  script:
    - mvn test

# Job to generate code coverage report
code_coverage:
  stage: quality
  image: maven:3.6.3-jdk-11
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn clean verify sonar:sonar -Dsonar.projectKey=your_project_key -Dsonar.organization=your_organization -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_TOKEN

# Job to build the Docker image
package:
  stage: package
  script:
    - docker build -t your-docker-image-name .

# Job to deploy the Docker image to Kubernetes
deploy:
  stage: deploy
  script:
    - echo "$KUBE_CA_PEM" > ca.pem
    - echo "$KUBE_TOKEN" > token
    - kubectl config set-cluster $KUBE_CLUSTER_NAME --server=$KUBE_SERVER --certificate-authority=ca.pem
    - kubectl config set-credentials gitlab-ci --token=$(cat token)
    - kubectl config set-context default --cluster=$KUBE_CLUSTER_NAME --user=gitlab-ci
    - kubectl config use-context default
    - kubectl set image deployment/$KUBE_DEPLOYMENT_NAME your_container_name=your-docker-image-name:latest -n $KUBE_NAMESPACE

# Job to send success email notification
success_email:
  stage: deploy
  script:
    - echo "Send success email"

# Job to send failure email notification
failure_email:
  stage: deploy
  script:
    - echo "Send failure email"
  when: on_failure

# Notify on success and failure
notify:
  email:
    recipients:
      - success@example.com
      - failure@example.com
    on_success: always
    on_failure: always